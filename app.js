'use strict';

/* Function to shorten query by ID and by Class */
function Id(Name) {
    return document.getElementById(Name);
}
function Class(Name) {
    return document.getElementsByClassName(Name);
}

/* Password verification */
function passwordVerification() {
    event.preventDefault();
    const modal = Id('modalWindow');
    const span = Class('close')[0];
    const secondInputField = Class('input-wrapper')[1];
    const errors = document.querySelectorAll('.invalid');
    errors.forEach(element => {
        element.remove();
    });

    if (Id('pass-input').value === '' || Id('confirm-pass-input').value === '') {
        secondInputField.insertAdjacentHTML('beforeend', '<p class="invalid">Поля не могут быть пустыми!</p>')
    } else if (Id('pass-input').value === Id('confirm-pass-input').value) {
        modal.style.display = 'block';
    } else {
        secondInputField.insertAdjacentHTML("beforeend", '<p class="invalid">Нужно ввести одинаковые значения.</p>');
    }

    /* Close modal window */
    span.onclick = function () {
        modal.style.display = 'none';
    }

    window.onclick = function (ev) {
        if (ev.target === modal) {
            modal.style.display = 'none';
        }
    }
}

/* Toggle password visibility */
function toggleViewPasswordTop() {
    let passInput = Id('pass-input');
    let passStatus = Id('pass-status');

    if (passInput.type === "password") {
        passInput.type = 'text';
        passStatus.className = "fas fa-eye-slash";
    } else {
        passInput.type = "password";
        passStatus.className = "fas fa-eye";
    }
}

function toggleViewPasswordBottom() {
    let passInput = Id('confirm-pass-input');
    let passStatus = Id('confirm-pass-status');

    if (passInput.type === "password") {
        passInput.type = 'text';
        passStatus.className = "fas fa-eye-slash";
    } else {
        passInput.type = "password";
        passStatus.className = "fas fa-eye";
    }
}